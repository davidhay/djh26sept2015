Palindromes
===========
This command line application finds the 3 longest unique palindromes in a supplied input string.

For the 3 longest palindromes, the palindrome, its start index and its length, will be output in descending order of palindrome length.

Example Output
--------------
Given the input string: **sqrrqabccbatudefggfedvwhijkllkjihxymnnmzpop** , the output will be:

    Text: hijkllkjih, Index: 23, Length: 10
    
    Text: defggfed, Index: 13, Length: 8
    
    Text: abccba, Index: 5 Length: 6

How to compile, test and run the program
----------------------------------------

The program can be downloaded from bitbucket with the following command

    $ git clone https://davidhay@bitbucket.org/davidhay/djh26sept2015.git

The program can be compiled with sbt

    $ sbt compile

The tests can be run with sbt

    $ sbt test

The program can be run via sbt

    $ sbt "run-main com.davidhay.Palindromes"


Assumptions
-----------

* The program is case sensitive.
* The program does not filter out blank space in the input.
* The program's input is expected to be supplied via the command line.
* Trivial palindromes which consist of a single character are ignored.