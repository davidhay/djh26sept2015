name := """davidhay-palindromes"""

version := "0.1.0"

scalaVersion := "2.11.2"

// ScalaTest
libraryDependencies += "org.scalatest" %% "scalatest" % "2.2.2" % "test"

// set the main class for packaging the main jar
mainClass in (Compile, packageBin) := Some("com.davidhay.Palindromes")

