package com.davidhay

/**
 * Created by davidhay on 26/09/2015.
 */

/**
 * Holds a palindrome and where it was found
 * @param palindrome the palindrome
 * @param index where the palindrome was found in the original input
 */
case class PalindromeInfo(val palindrome: String, index: Int) {

  require(palindrome.isEmpty == false)
  require(Palindromes.isPalindrome(palindrome))
  require(index >= 0)

  val length = palindrome.length

  override def toString = {
    s"PalindromeInfo($palindrome,length=[$length],index=[$index])"
  }

}

object Palindromes {

  import scala.annotation.tailrec

  def isPalindrome(value: String) = value == value.reverse

  def getAllPalindromes(input: String): Seq[String] = {
    val startIndexes = 0 until input.length
    startIndexes.flatMap { startIdx => {
      val endIndxes = startIdx + 1 to input.length
      val candiates = endIndxes.map { endIdx => input.substring(startIdx, endIdx) }
      candiates filter isPalindrome
    }
    }
  }

  def getUnique(values: Seq[String]): Seq[String] = {
    values.groupBy(value => value)
      .map { case (pal, instances) => (pal, instances.length) }
      .filter { case (pal, count) => count == 1 }
      .map { case (pal, _) => pal }
      .toSeq
  }

  @tailrec
  def removeContained(palindromes: List[String], acc: List[String] = List.empty): List[String] = {
    def isContainedWithin(longer: String)(value: String) = longer.contains(value)
    palindromes match {
      case h :: t => removeContained(t filterNot isContainedWithin(h), h :: acc)
      case Nil => acc.reverse
    }
  }

  def getPalindromeInfo(input: String): Seq[PalindromeInfo] = {
    val pals = getAllPalindromes(input)
    val nonTrivialPals = pals.filter(_.length > 1)
    val unique = getUnique(nonTrivialPals)

    val nonContainedPals = removeContained(unique.toList)
    val longest3 = nonContainedPals.sortWith(_.length > _.length).take(3)
    longest3.map { pal => PalindromeInfo(pal, input.indexOf(pal)) }
  }

  def main(args: Array[String]): Unit = {

    val standardIn = System.console()

    println("Please enter a text value from which the 3 longest unique palindromes will be extracted.")
    print("value> ")
    val value = standardIn.readLine()
    if (value.isEmpty) {
      println("You did not enter a value.")
    } else {
      val pals = getPalindromeInfo(value)
      if (pals.isEmpty) {
        println("There were no palindromes found.")
      } else {
        val lines = pals.map {
          case pal@PalindromeInfo(palindrome, index) =>
            s"Text: ${palindrome}, Index: ${index}, Length: ${pal.length}"
        }
        println
        lines map { line => line + "\n" } map println
      }
    }
    println("Program Ended.")
  }

}
