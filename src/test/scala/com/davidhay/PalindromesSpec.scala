package com.davidhay

import org.scalatest.FlatSpec
import org.scalatest.Matchers

/**
 * Created by davidhay on 26/09/2015.
 */

class PalindromesSpec extends FlatSpec with Matchers {

  import Palindromes._

  {
    def checkPalindromes(value: String, expectedPalindrome: Boolean) = {
      val not = if (expectedPalindrome) "" else "not"
      s"The value [$value]" should s"$not be a palindrome " in {
        isPalindrome(value) should be(expectedPalindrome)
      }
    }

    checkPalindromes("", true)
    checkPalindromes("a", true)
    checkPalindromes("aa", true)
    checkPalindromes("asdfdsa", true)
    checkPalindromes("abc cba", true)
    checkPalindromes("12", false)
    checkPalindromes("121 ", false)
  }

  {
    def checkAllPalindromes(value: String, expectedPalindromes: Set[String]) = {
      s"The palindromes contained within [$value]" should s"be $expectedPalindromes " in {
        val pals = getAllPalindromes(value).toSet
        println(s"PALS FROM $value are $pals")
        pals should be(expectedPalindromes)
      }
    }

    checkAllPalindromes("", Set())
    checkAllPalindromes("a", Set("a"))
    checkAllPalindromes("ab", Set("a", "b"))
    checkAllPalindromes("aba", Set("a", "b", "aba"))
    checkAllPalindromes("abacab", Set("a", "b", "c", "aba", "aca", "bacab"))
  }

  {
    def checkUnique(values: Seq[String], expectedUnique: Seq[String]) = {
      s"The unique values in  [$values] of length ${values.length}" should s"be $expectedUnique " in {
        getUnique(values) should be(expectedUnique)
      }
    }

    checkUnique(Seq(), Seq())
    checkUnique(Seq(""), Seq(""))
    checkUnique(Seq("one"), Seq("one"))
    checkUnique(Seq("one", "one"), Seq())
    checkUnique(Seq("one", "one", "two", "three", "three"), Seq("two"))

  }
  {
    def checkRemoveContained(values: List[String], expected: List[String]) = {
      s"When [${values}] is filtered to remove any value which is a substring of a previous value, the result" should s"be [$expected] " in {
        removeContained(values) should be(expected)
      }
    }

    checkRemoveContained(List(), List())
    checkRemoveContained(List("a", "b", "c"), List("a", "b", "c"))
    checkRemoveContained(List("aa", "b", "c", "a"), List("aa", "b", "c"))
    checkRemoveContained(List("aa", "b", "c", "cc"), List("aa", "b", "c", "cc"))

  }

  {
    def checkGetPalindromeInfo(value: String, expected: Seq[PalindromeInfo]) = {
      s"The longest 3 unique palindromes extracted from ${value} sorted by length decending" should s"be [$expected] " in {
        getPalindromeInfo(value) should be(expected)
      }
    }

    checkGetPalindromeInfo("aba", Seq(PalindromeInfo("aba", 0)))
    checkGetPalindromeInfo("aba_cddc_efffe", Seq(
      PalindromeInfo("_cddc_", 3),
      PalindromeInfo("efffe", 9),
      PalindromeInfo("aba", 0)
    ))

    checkGetPalindromeInfo("sqrrqabccbatudefggfedvwhijkllkjihxymnnmzpop", Seq(
      PalindromeInfo("hijkllkjih", 23),
      PalindromeInfo("defggfed", 13),
      PalindromeInfo("abccba", 5)
    ))
  }

  {
    def checkIllegalArgs(value: String, index: Int) = {
      s"illegal arguments [$value] and [$index] used to construct a PalindromeInfo instance" should "fail " in {
        intercept[IllegalArgumentException] {
          PalindromeInfo(value, index)
          fail("exception expected!")
        }
      }
    }
    checkIllegalArgs("word", 0)
    checkIllegalArgs("abba", -1)

  }

}
